============================
CERN Ironic Hardware Manager
============================

This element is a wrapper around ironic-agent_. It will install cern-ironic-hardware-manager_ in IPA's venv before starting ironic-python-agent, so the code is available during runtime.
Please note it does not hardcode HWmanager inside the image and will always download the up-to-date version from GitLab repository.

Example code used to build a deploy image is shown bellow

.. code-block:: shell

    ELEMENTS_PATH=~/cci-elements \
    DIB_REPOREF_ironic_agent=stable/ocata \
    disk-image-create -o ironic-deploy-image-ocata \
    	ironic-hwmanager-cern \
    	local-config \
        disable-selinux

.. _ironic-agent: https://github.com/openstack/diskimage-builder/tree/master/diskimage_builder/elements/ironic-agent
.. _cern-ironic-hardware-manager: https://gitlab.cern.ch/cloud-infrastructure/cern-ironic-hardware-manager
